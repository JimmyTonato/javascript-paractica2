/*  
  @name= iframes-1.js
  @author= Jimmy Daniel Tonato Benavides
  @version= 1.0
  @description= Controller associated to iframe-1.html 
  @date = 04-11-2019
*/
/*  
  @name= window.onload
  @author= Jimmy Daniel Tonato Benavides
  @version= 1.0
  @description= Event that captures when the page is loaded. 
                It's used to hide/show some components.
  @date = 04-11-2019
  @params= none
  @return = none
*/
window.onload = function() {
        this.inicial();
    }
    /*  
      @name= window.onload
      @author= Jimmy Daniel Tonato Benavides
      @version= 1.0
      @description= Event that captures when the page is loaded. 
                    It's used to hide/show some components.
      @date = 04-11-2019
      @params= none
      @return = none
    */
function inicial() {
    $('#errorInput').hide();
    $('#errorSelect').hide();
}
/*  
  @name= verificar()
  @author= Jimmy Daniel Tonato Benavides
  @version= 1.0
  @description= We collect the select and the input to verify it  
  @date = 04-11-2019
  @params= none
  @return = none
*/
function verificar() {
    var selects = document.getElementById('select').value;
    var inputs = document.getElementById('inpu').value;
    var validoSelect = errorSelect(selects);
    var validoInput = errorInput(inputs);

    //we verified each of the returns given to us by the previous functions
    if (validoInput == true && validoSelect == true) {
        window.parent.resultado();
    }
}
/*  
  @name= errorSelect(write)
  @author= Jimmy Daniel Tonato Benavides
  @version= 1.0
  @description= 
  @date = 04-11-2019
  @params= write(select with your value)
  @return = true or false
*/
function errorSelect(write) {
    if (write == "0") {
        $('#errorSelect').show();
        return false;
    } else {
        $('#errorSelect').hide();
        return true
    }
}
/*  
  @name= errorInput(write) 
  @author= Jimmy Daniel Tonato Benavides
  @version= 1.0
  @description= 
  @date = 04-11-2019
  @params= write(input with your value)
  @return = true o false
*/
function errorInput(write) {
    if (write.toString().trim() == "" || isNaN(write) || write <= 0) {
        $('#errorInput').show();
        return false;
    } else {
        $('#errorInput').hide();
        return true;
    }
}