/*  
  @name= popUpWindows.js
  @author= Jimmy Daniel Tonato Benavides
  @version= 1.0
  @description= Controller associated to popUpWindows.html
  @date = 04-11-2019
*/
/*  
  @name= loadData()
  @author= Jimmy Daniel Tonato Benavides
  @version= 1.0
  @description= we start with this function 
  @date = 04-11-2019
  @params= none
  @return = none
*/
function loadData() {
    var d = new Date();
    document.getElementById("time").innerHTML = "Time: " + d.toDateString();
    createTable();

}
/*  
  @name= createTable()
  @author= Jimmy Daniel Tonato Benavides
  @version= 1.0
  @description= With the previous function we call it and what it 
  does is to make the table and fill it with the data.
  @date = 04-11-2019
  @params= none
  @return = none
*/
function createTable() {
    var valueInput = window.opener.frames[0].document.getElementById('inpu').value;
    var tabla = "<table class='ui fixed table'>";
    tabla += "<thead><tr><th>PRODUCT NAME</th><th>CODE</th><th>TEST</th></tr></thead>"
    var test = "";
    tabla += "<tbody>";
    for (let i = 0; i < valueInput; i++) {
        if (window.opener.document.getElementById("check" + i).value == "true") {
            test = "<td class='positive'><i class='icon checkmark'></i> YES</td>"
        } else {
            test = "<td class='negative'><i class='icon close'></i>NO</td>"
        }
        tabla += "<tr>";
        tabla += "<td>" + window.opener.document.getElementById("inputName" + i).value + "</td>";
        tabla += "<td>" + window.opener.document.getElementById("inputSecuencia" + i).value + "</td>";
        tabla += test;
        tabla += "</tr>";
    }
    tabla += "</tbody></table>";
    document.getElementById("database").innerHTML = tabla;
    document.getElementById("database").innerHTML += "Total Products introduced in the Database: " + valueInput;
}
/*  
  @name= print2()
  @author= Jimmy Daniel Tonato Benavides
  @version= 1.0
  @description= we call the function window.print();
  @date = 04-11-2019
  @params= none
  @return = none
*/
function print2() {
    window.print();
}
/*  
  @name= closeWindow()
  @author= Jimmy Daniel Tonato Benavides
  @version= 1.0
  @description= we call the function window.close();
  @date = 04-11-2019
  @params= none
  @return = none
*/
function closeWindow() {
    window.close();
}