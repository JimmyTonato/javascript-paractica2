/*  
  @name= index.js
  @author= Jimmy Daniel Tonato Benavides
  @version= 1.0
  @description= Controller associated to index.html
  @date = 04-11-2019
*/
/*  
  @name= window.onload
  @author= Jimmy Daniel Tonato Benavides
  @version= 1.0
  @description= we initialize with a function
  @date = 04-11-2019
  @params= none
  @return = none
*/
window.onload = function() {

        this.inicial();
    }
    /*  
      @name= inicial()
      @author= Jimmy Daniel Tonato Benavides
      @version= 1.0
      @description= This is the function of the design of the first 
      cover before giving a click
      @date = 04-11-2019
      @params= none
      @return = none
    */
function inicial() {
    const tl = new TimelineMax()
    TweenMax.delayedCall(0, function() {
        tl.fromTo(".hero", 1, { height: "0%" }, { height: "80%", ease: Power2.easeInOut })
            .fromTo(".hero", 1.2, { width: "100%" }, { width: "80%", ease: Power2.easeInOut })
            .fromTo(".slider", 1.2, { x: "-100%" }, { x: "0%", ease: Power2.easeInOut }, "-=1.2")
            .fromTo(".headLine", 0.7, { opacity: 0, x: 30 }, { opacity: 1, x: 0 }, "-=0.5");
    });
}
/*  
  @name= inicio()
  @author= Jimmy Daniel Tonato Benavides
  @version= 1.0
  @description= Function where once given the click
   will take us to the iframe to select
  @date = 04-11-2019
  @params= none
  @return = none
*/
function inicio() {
    moverImagen();
    girarImagen();
    document.getElementById("caja1").hidden = false;
    $('.frame1').hide();
    $('.frame1').show(4000);
}
/*  
  @name= moverImagen()
  @author= Jimmy Daniel Tonato Benavides
  @version= 1.0
  @description= Function to move and make smaller the 
  image that was before background
  @date = 04-11-2019
  @params= none
  @return = none
*/
function moverImagen() {
    const tl = new TimelineMax()
    document.getElementById("index").style.display = "none";
    TweenMax.delayedCall(0, function() {
        tl.fromTo("section", 2, { width: "40%" }, { width: "25%", ease: Power2.easeInOut })
    });
    new TweenMax("section", 3.5, { x: "25%" });
    TweenMax.to(".headLine h1", 0.2, { fontSize: "260%", lineHeight: '150%', ease: Sine.easeOut, autoRound: true, delay: 0 });
    new TweenMax("section", 3.5, { y: "-3%" });
}
/*  
  @name= girarImagen()
  @author= Jimmy Daniel Tonato Benavides
  @version= 1.0
  @description= function where we rotate the image each 
  time we pass the mouse
  @date = 04-11-2019
  @params= none
  @return = none
*/
function girarImagen() {
    document.getElementById("imagen").addEventListener('mouseover', btnHandler, false);
    var rotation = 0;
    var tween;

    function btnHandler() {
        if (tween && tween.isActive()) return;
        tween = TweenMax.to("#imagen", 3, { rotation: rotation -= 360 });
    }
}
/*  
  @name= moverIcono()
  @author= Jimmy Daniel Tonato Benavides
  @version= 1.0
  @description= Function where we move the icoono to the right side
  @date = 04-11-2019
  @params= none
  @return = none
*/
function moverIcono() {
    new TweenMax("#pro", 3, { x: "260%" });
}
/*  
  @name= 
  @author= Jimmy Daniel Tonato Benavides
  @version= 1.0
  @description= Function where we create imputs with the given value
  @date = 04-11-2019
  @params= none
  @return = none
*/
function resultado() {
    document.getElementById("errores").style.display = "none";
    moverIcono();
    document.getElementById("caja2").hidden = false;
    document.getElementById("ifrimes-1").style.display = "none";
    var valueInput = window.frames[0].document.getElementById('inpu').value;
    var valueSelect = window.frames[0].document.getElementById('select').value;
    document.getElementById("titulo").innerText = "Enter Products For Category " + valueSelect + " Code";
    createInputs(valueInput);
}
/*  
  @name= createInputs
  @author= Jimmy Daniel Tonato Benavides
  @version= 1.0
  @description= Function that makes a loop to create each input with a different id
  @date = 04-11-2019
  @params= number(the number of inputs to create)
  @return = none
*/
function createInputs(number) {
    for (let i = 0; i < number; i++) {
        document.getElementById("datos").innerHTML += "<div class='cajainputs'><div class='ui input'>" +
            "<input id=inputName" + i + " style='margin-right: 20px;' type='text' placeholder='Number..'>" +
            "<input id=inputSecuencia" + i + " type='text' placeholder='Number..'><div id='checkbox1' " +
            "class='custom-control custom-checkbox'><input type='checkbox' class='custom-control-input' " +
            "id=check" + i + " value='false' onclick='checkTrue(" + i + ")'><label " +
            "class='custom-control-label' for=check" + i + "></label></div></div></div>";

    }
}
/*  
  @name= checkTrue
  @author= Jimmy Daniel Tonato Benavides
  @version= 1.0
  @description= It works where every time I give him a checkbox 
  I put him as true.
  @date = 04-11-2019
  @params= number(It's your id number)
  @return = none
*/
function checkTrue(number) {
    document.getElementById("check" + number).value = true;
}
/*  
  @name= verificar()
  @author= Jimmy Daniel Tonato Benavides
  @version= 1.0
  @description= where we verify that all the data you are entering are correct 
  @date = 04-11-2019
  @params= none
  @return = none
*/
function verificar() {
    document.getElementById("errores").innerHTML = "";
    var valueInput = window.frames[0].document.getElementById('inpu').value;
    var valueSelect = window.frames[0].document.getElementById('select').value;
    var error1 = 0;
    var error2 = 0;
    if (valueSelect == "DNA") {
        var regex = /[^AGCT]/i;
    } else {
        var regex = /[^AGCU]/i;
    }
    for (let i = 0; i < valueInput; i++) {
        document.getElementById("errores").innerHTML = "";
        name = document.getElementById("inputName" + i).value;
        secuencia = document.getElementById("inputSecuencia" + i).value;
        if (name.toString().trim() == "") {
            document.getElementById("errores").style.display = "block";
            document.getElementById("errores").innerHTML = "<p>One of the names is wrong. Check it out</p>";
            document.getElementById("inputName" + i).style.backgroundColor = "rgb(220, 104, 104)";
            document.getElementById("inputName" + i).style.color = "white";
            document.getElementById("inputName" + i).style.border = "solid #FFC107 0.1px";
            document.getElementById("inputName" + i).style.boxShadow = "0 0 4px rgb(220, 104, 104)"
        } else {
            error1 += 1;
            document.getElementById("errores").style.display = "none";
            document.getElementById("inputName" + i).style.backgroundColor = null;
            document.getElementById("inputName" + i).style.border = null;
            document.getElementById("inputName" + i).style.color = null;
            document.getElementById("inputName" + i).style.boxShadow = null;
        }
        if (regex.test(secuencia) == true || secuencia.toString().trim() == "") {
            document.getElementById("errores").style.display = "block";
            document.getElementById("errores").innerHTML += "<p>One of the sequences is wrong. Check it out.(" + valueSelect + ")</p>";
            document.getElementById("inputSecuencia" + i).style.backgroundColor = "rgb(220, 104, 104)";
            document.getElementById("inputSecuencia" + i).style.color = "white";
            document.getElementById("inputSecuencia" + i).style.border = "solid #FFC107 0.1px";
            document.getElementById("inputSecuencia" + i).style.boxShadow = "0 0 4px rgb(220, 104, 104)";

        } else {
            error2 += 1;
            document.getElementById("errores").style.display = "none";
            document.getElementById("inputSecuencia" + i).style.backgroundColor = null;
            document.getElementById("inputSecuencia" + i).style.border = null;
            document.getElementById("inputSecuencia" + i).style.color = null;
            document.getElementById("inputSecuencia" + i).style.boxShadow = null;
        }
        if (error1 == valueInput && error2 == valueInput) {
            var decision = confirm("You really want to send");
            if (decision) {
                window.open("../popUpWindows/popUpWindow.html", "_blank", "width=1000px, height=400px");
            }
        }
    }
}